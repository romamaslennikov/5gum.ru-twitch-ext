import Vue from 'vue';
import VueNativeSock from 'vue-native-websocket';
import VueNoty from 'vuejs-noty';
import vClickOutside from 'v-click-outside';
import Vuelidate from 'vuelidate';
import svg4everybody from 'svg4everybody';
import { VLazyImagePlugin } from 'v-lazy-image';
import * as dayjs from 'dayjs';
import 'dayjs/locale/ru';
import App from './App.vue';
import router from './router';
import store from './store';
import './utils/filters';

// global components
import Img from './components/Img.vue';
import SvgIcon from './components/SvgIcon.vue';

// global CSS
import 'vuejs-noty/dist/vuejs-noty.css';
import 'normalize.css/normalize.css'; //  reset CSS
import '@/styles/layout.sass';

// ie11 support
import { isIE11 } from './utils/device';

if (isIE11) { // ie11 support
  require('es6-shim'); // eslint-disable-line
  require('element-closest-polyfill'); // eslint-disable-line
  document.write('<script src="https://cdn.jsdelivr.net/gh/nuxodin/ie11CustomProperties@4.1.0/ie11CustomProperties.min.js"></script>'); // css vars
}

// for svg support
require('intersection-observer'); // for svg

Vue.config.productionTip = false;

Vue.use(VueNoty);
Vue.use(Vuelidate);
Vue.use(vClickOutside);
svg4everybody();
Vue.use(VLazyImagePlugin);

Vue.component('Img', Img);
Vue.component('SvgIcon', SvgIcon);

Vue.prototype.$date = dayjs;
Vue.prototype.$date.locale('ru');

const ws = process.env.VUE_APP_BASE_API_WS;

Vue.use(VueNativeSock, ws, {
  store,
  connectManually: true,
  reconnection: true,
  reconnectionDelay: 1000,
  format: 'json',
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
