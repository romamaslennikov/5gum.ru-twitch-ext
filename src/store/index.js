import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

// Modules
import app from './modules/app';
import me from './modules/me';
import admin from './modules/admin';
import socket from './modules/socket';
import quiz from './modules/quiz';
import popup from './modules/popup';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    app,
    me,
    socket,
    quiz,
    popup,
    admin,
  },
  strict: process.env.DEV,
  plugins: [createPersistedState({
    key: process.env.VUE_APP_STATE_KEY,
  })],
});

export default store;
