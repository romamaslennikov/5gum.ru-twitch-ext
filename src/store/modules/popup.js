let resolve;

const popup = {
  namespaced: true,

  state: {
    popup: {
      opened: null,
      openedPopupContent: null,
      currentPopup: null,
      disabled: null,
      message: {
        head: null,
        body: null,
        btn: null,
      },
      data: null,
    },
  },

  getters: {
    popup: (state) => state.popup,
  },

  mutations: {
    SHOW_POPUP_CONTENT: (state, open) => {
      state.popup.openedPopupContent = open;
    },

    SHOW_POPUP: (state, obj) => {
      state.popup.opened = obj.opened;
      state.popup.currentPopup = obj.currentPopup;
      state.popup.message = obj.message;
      state.popup.data = obj.data;
      state.popup.disabled = obj.disabled;
    },
  },

  actions: {
    async show({ commit }, data) {
      commit('SHOW_POPUP', {
        opened: true,
        ...data,
      });

      const a = await new Promise((r) => {
        resolve = r;
      });

      return a;
    },

    hide({ commit, state }, data) {
      commit('SHOW_POPUP_CONTENT', null);

      if (resolve) {
        resolve(data);
      } else {
        state.popup.opened = null;
      }
    },
  },
};

export default popup;
