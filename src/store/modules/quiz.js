import { answer, ratingGet } from '@/api/me';

const quiz = {
  namespaced: true,

  state: {
    quiz: null,
    time: null,
    result: null,
    rating: null,
    morePrev: [],
    moreNext: [],
  },

  getters: {
    quiz: (state) => state.quiz,
    time: (state) => state.time,
    result: (state) => state.result,
    rating: (state) => state.rating,
  },

  mutations: {
    UPDATE_RATING: (state, o) => {
      state.rating = o;
    },

    UPDATE_RESULT: (state, o) => {
      state.result = o;
    },

    UPDATE_QUIZ: (state, o) => {
      state.quiz = o;
    },

    UPDATE_TIME: (state, o) => {
      state.time = o;
    },
  },

  actions: {
    async RatingGet(o, data) {
      const res = await ratingGet(data);

      return res;
    },

    async Answer(o, data) {
      const res = await answer(data);

      return res;
    },
  },
};

export default quiz;
