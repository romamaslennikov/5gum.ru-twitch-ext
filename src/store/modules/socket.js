const socket = {
  state: {
    isConnected: false,
    reconnectError: false,
    online: 0,
  },

  getters: {
    isConnected: (state) => state.isConnected,
    online: (state) => state.online,
  },

  mutations: {
    SOCKET_ONOPEN(state) {
      state.isConnected = true;

      console.log('---', 'SOCKET_ONOPEN');
    },

    SOCKET_ONCLOSE(state, event) {
      state.isConnected = false;

      console.log('---', 'SOCKET_ONCLOSE');

      console.log('---', event);
    },

    SOCKET_ONERROR(state, event) {
      state.isConnected = false;

      console.log('---', 'SOCKET_ONERROR');

      console.log('---', event);
    },

    SOCKET_ONMESSAGE(state, message) {
      console.log('---', 'SOCKET_ONMESSAGE');

      console.log(message);

      if (message.name === 'online.count') {
        state.online = message.data.online;
      }
    },

    SOCKET_RECONNECT(state) {
      state.isConnected = false;

      console.log('---', 'SOCKET_RECONNECT');
    },

    SOCKET_RECONNECT_ERROR(state) {
      state.reconnectError = true;
    },
  },
  actions: {},
};

export default socket;
