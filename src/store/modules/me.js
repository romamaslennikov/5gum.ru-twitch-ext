import loginTwitch from '@/api/login';
import { listen, promocode, entries } from '@/api/me';
import { getToken, setToken, removeToken } from '@/utils/auth';

const me = {
  namespaced: true,

  state: {
    token: getToken(), // uuid c твича
    user: null,
  },

  getters: {
    token: (state) => state.token,
    user: (state) => state.user,
  },

  mutations: {
    SET_USER: (state, user) => {
      state.user = user;
    },

    SET_TOKEN: (state, token) => {
      state.token = token;

      setToken(token);
    },

    REMOVE_TOKEN: (state) => {
      state.token = null;

      removeToken();
    },
  },

  actions: {
    Entries(o, data) {
      return (async () => entries(data))();
    },

    async LoginTwitch(o, data) {
      const res = await loginTwitch(data);

      return res;
    },

    async Listen(o, data) {
      const res = await listen(data);

      return res;
    },

    async Promocode(o, data) {
      const res = await promocode(data);

      return res;
    },
  },
};

export default me;
