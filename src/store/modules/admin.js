import { adminQuizStart, adminQuizClose, adminQuizReset } from '@/api/admin';

const me = {
  namespaced: true,

  state: {
    quizList: null,
  },

  getters: {
    quizList: (state) => state.quizList,
  },

  mutations: {
    SET_LIST: (state, o) => {
      state.quizList = o;
    },
  },

  actions: {
    async AdminQuizStart(o, data) {
      const res = await adminQuizStart(data);

      return res;
    },

    async AdminQuizClose() {
      const res = await adminQuizClose();

      return res;
    },

    async AdminQuizReset() {
      const res = await adminQuizReset();

      return res;
    },
  },
};

export default me;
