const routes = [
  {
    path: '/',
    component: () => import('../layouts/Layout.vue'),
    children: [
      { path: '', name: 'home', component: () => import('../views/Home.vue') },
      { path: '/start', name: 'start', component: () => import('../views/Start.vue') },
      { path: '/quiz', name: 'quiz', component: () => import('../views/Quiz.vue') },
      { path: '/result', name: 'result', component: () => import('../views/Result.vue') },
      { path: '/rss', name: 'rss', component: () => import('../views/Rss.vue') },
    ],
  },
  {
    path: '/',
    component: () => import('../layouts/LayoutFull.vue'),
    children: [
      { path: '/dashboard', name: 'dashboard', component: () => import('../views/Dashboard.vue') },
    ],
  },
  {
    path: '*',
    component: () => import('../layouts/Layout.vue'),
    children: [
      { path: '', component: () => import('../views/Home.vue') },
    ],
  },
];

export default routes;
