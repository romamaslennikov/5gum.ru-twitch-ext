export const isIE11 = !!window.MSInputMethodContext && !!document.documentMode;

export const isTouch = () => 'ontouchstart' in window || navigator.maxTouchPoints;

const searchToObject = () => {
  const pairs = window.location.search.substring(1)
    .split('&');
  const obj = {};
  let pair;
  let i;

  for (i in pairs) { // eslint-disable-line
    if (pairs[i] === '') continue; // eslint-disable-line

    pair = pairs[i].split('=');

    obj[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
  }

  return obj;
};

export const isPortrait = () => {
  const { platform } = searchToObject();

  if (platform === 'mobile') {
    return true;
  }

  return window.innerWidth / window.innerHeight <= 1.2;
};

export const iOS = () => [
  'iPad Simulator',
  'iPhone Simulator',
  'iPod Simulator',
  'iPad',
  'iPhone',
  'iPod',
].includes(navigator.platform)
    // iPad on iOS 13 detection
    || (navigator.userAgent.includes('Mac') && 'ontouchend' in document);

export default {
  isIE11,
  isTouch,
  isPortrait,
  iOS,
};
