import Vue from 'vue';

export function adminQuizStart(data) {
  Vue.prototype.$socket.sendObj({
    name: 'streamer.quiz.start',
    data: data.data,
  });
}

export function adminQuizClose() {
  Vue.prototype.$socket.sendObj({
    name: 'streamer.quiz.close',
  });
}

export function adminQuizReset() {
  Vue.prototype.$socket.sendObj({
    name: 'streamer.quiz.reset',
  });
}
