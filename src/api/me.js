import Vue from 'vue';
import qs from 'qs';
import request from '@/utils/request';

const timeout = 10 * 1000;

const promise = (method, data) => new Promise((resolve, reject) => {
  const { ctx } = data;

  ctx.$options.sockets.onmessage = (r) => {
    const message = JSON.parse(r.data);
    if (message?.name === method) {
      resolve(message.data);
    } else if (message?.name === 'error') {
      resolve(message.data);
    } else if (message.error) {
      resolve(message);
    }
  };

  setTimeout(() => reject(new Error(`Сокет не ответил на метод '${method}'`)), timeout);
});

export function entries(data) {
  const options = qs.stringify(data);

  return request({
    url: '/entries',
    method: 'post',
    data: options, // email
  });
}

export function ratingGet(data) {
  Vue.prototype.$socket.sendObj({
    name: 'rating.get',
    data: data.data,
  });

  return promise('rating', data);
}

export function promocode(data) {
  Vue.prototype.$socket.sendObj({
    name: 'promocode',
    data: data.data,
  });

  return promise('promocode', data);
}

export function listen(data) {
  Vue.prototype.$socket.sendObj({
    name: 'listen',
    data: data.data,
  });

  return promise('hello', data);
}

export function answer(data) {
  Vue.prototype.$socket.sendObj({
    name: 'answer',
    data: data.data,
  });
}
