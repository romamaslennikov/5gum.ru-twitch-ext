import Vue from 'vue';

const timeout = 10 * 1000;

const promise = (method, data) => new Promise((resolve, reject) => {
  const { ctx } = data;

  ctx.$options.sockets.onmessage = (r) => {
    const message = JSON.parse(r.data);
    if (message?.name === method) {
      resolve(message.data);
    } else if (message?.name === 'error') {
      resolve(message.data);
    }
  };

  setTimeout(() => reject(new Error(`Сокет не ответил на метод '${method}'`)), timeout);
});

export default function loginTwitch(data) {
  Vue.prototype.$socket.sendObj({
    name: 'login.jwt',
    data: data.data,
  });

  return promise('login.success', data);
}
